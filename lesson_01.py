# Однострочный комментарий
'''
Многострочный
    комментарий
'''
"""
            Многострочный
    комментарий
"""

# todo: Как объявить переменную?

is_student = True
price = 100.50
some_var = None
print(is_student)

"""
todo: Какие типы данных есть в Python?
Скалярные:
    bool, int, float, complex, str, bytes
Ссылочные (структурные):
    tuple, list, set, dict, объекты
Специальные:
    None

Неизменяемые (immutable): скалярные + tuple
Изменяемые (mutable): list, set, dict, объекты
"""

# todo: bool
flag1 = True
flag2 = False

# todo: int
i1 = 666
i2 = -777
i3 = 0b101010
i4 = 0o755
i5 = 0xff
i6 = 0xFF

# todo: float
f1 = 1.23
f2 = -1.23
f3 = 12e6  # 12 * 10 ** 6  => 12000000.0
f4 = 12e-3 # 12 * 10 ** -3 => 0.012

# todo: complex
c1 = complex(3, 2)    # 3+2j
c2 = 3+2j             # complex(3, 2)
c3 = 3.14j            # complex(0, 3.14) 0+3.14j
print(c3.real)        # действительная часть
print(c3.imag)        # мнимая часть
print(c3.conjugate()) # сопряженное число

# todo: str
s1 = 'Hello, Python!\n'
s2 = "Hello, Python!"
s2 = "Hello, \" ' Python!"
s2 = 'Hello, " \' \\Python!'
s2 = 'Hello, " Python!'
s3 = '''
    Текст
        1
            2
'''
s4 = """Текст"""
s5 = r'^\d+$'

# todo: bytes
b1 = b'Hello, Python'
b2 = bytes('Привет', 'utf-8')

# todo: tuple
person = ('Иванов', 'Иван', 50, ('python', 'js'))
print(person[1], person[3][1])
# person[1] = 'Сидр'
person = ('Иванов', 'Сидр', 50, ('python', 'js'))

# todo: list
l1 = [1, 2, 3, [4, 5]]
print(l1[3][0])
l1[2] = 666
l1.append(777)    # O(1)
l1.insert(0, 888) # !!!!!!!! O(n)
print(l1)

# todo: set
ss = {1, 2, 3, 1, 2, 3}
ss.add(5)
ss.update({10, 10, 11})
ss2 = set() # empty set

# todo: dict
d = {} # empty dict
person = {
    'firstname': 'Иван',
    'lastname': 'Иванов',
    'age': 50,
    'skills': ['js', 'python'],
}
print(person['firstname'])
person['middlename'] = 'Иванович'


# PEP-8


"""
todo: Какие операторы существуют в Python?

Арифметические:  + - * / % // **
Сравнения:       == != > < >= <=
Присваивания:    = += -= *= /= %= //= **=
Логические:      and or not
Побитовые:       & | ~ ^ << >>
Принадлежности:  in, not in
Тождественности: is, not is
"""

print('middlename' in person) # O(1)
print(5 in l1)                # O(n)


# todo: Как определить тип переменной в Python?
print(type(person))

# todo: Как выполнить явное приведение
# переменной к определенному типу?
print(int('45'), float('1.5'))



# todo: Ветвление

a = input()
b = input()
if a < b:
    print('a < b'
    )
elif a == b:
    print('a = b')
else:
    print('a > b')
    #assert False, 'Мы никогда сюда не должны попасть'

"""
todo: Тернарный оператор
password = ''
if password:
    password += '!'
else:
    password = None
"""
password = ''
password = password + '!' if password else None
print(password)

# todo: циклы
# break и continue

i = 0

while i < 5:
    print(i)
    i += 1

j = 1

while j:
    if j == 10:
        break
    j += 1


lst = [1, 2, 3]

for i in lst:
    print('=>', i)

s = 'Hello, Python!'

for i, c in enumerate(s):
    print('Index:', i, 'Char:', c)


for key, value in person.items():
    print(key, value)
